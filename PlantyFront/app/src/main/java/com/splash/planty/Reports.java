package com.splash.planty;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Reports extends AppCompatActivity {

    private TextView date_created;
    private TextView diagonosis;
    ImageView ImageScanned;
    private Report r;

    LinearLayout l;
    ImageView iv;
    TextView tv;
    int yellow = Color.parseColor("#ffcc00");
    int green = Color.parseColor("#0F9D58");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        date_created = (TextView)findViewById(R.id.created_date);
        diagonosis = (TextView)findViewById(R.id.diagonosis);
        ImageScanned = (ImageView)findViewById(R.id.image_scan);
        l = (LinearLayout) findViewById(R.id.mainlayout);
        iv = (ImageView) findViewById(R.id.ivmessage);
        tv = (TextView) findViewById(R.id.tvmessage);


        l.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        r = (Report)getIntent().getSerializableExtra("report");

        date_created.setText( r.getCreated_date());
        diagonosis.setText(r.getDiagnosis());
        Log.d("Radwan", "image url is "+ r.getImage());
        Bitmap myBitmap = getIntent().getParcelableExtra("image");
        ImageScanned.setImageBitmap(myBitmap);

        if(r.getDiagnosis().toLowerCase().contains("healthy"))
        {l.setVisibility(View.VISIBLE);
            l.setBackgroundColor(green);
            iv.setImageDrawable(getResources().getDrawable(R.drawable.tick));
            tv.setText("Plant is healthy!");}
        else
        {l.setVisibility(View.VISIBLE);

            l.setBackgroundColor(yellow);
            iv.setImageDrawable(getResources().getDrawable(R.drawable.warning));
            tv.setText("Plant is infected!");
        }

    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, Home.class);
        startActivity(i);
    }

}
