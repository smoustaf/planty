package com.splash.planty;

import java.io.Serializable;

public class Report implements Serializable {

        public Report(String created_date, String image, String diagnosis) {

            this.created_date = created_date;
            this.image = image;
            this.diagnosis = diagnosis;
        }


        private String created_date;
        private String image;
        private String diagnosis;



        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setDiagnosis(String diagnosis) {
            this.diagnosis = diagnosis;
        }


        public String getCreated_date() {
            return created_date;
        }

        public String getImage() {
            return "http://10.0.2.2:8000/"+image;
        }

        public String getDiagnosis() {
            return diagnosis.replace("_"," ");
        }

    }
