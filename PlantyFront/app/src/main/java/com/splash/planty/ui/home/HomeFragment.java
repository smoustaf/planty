package com.splash.planty.ui.home;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.splash.planty.ImageChooser;
import com.splash.planty.R;
import com.splash.planty.Report;
import com.splash.planty.ReportsAdapter;
import com.splash.planty.User;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    LayerDrawable icon ;
    private ImageView plusImg;
    private TextView welcome;
    public User testUser;
    public ArrayList<Report> list;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "CardViewActivity";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        homeViewModel.getText().observe(this, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        plusImg  = (ImageView) root.findViewById(R.id.bigPlusImage);
        list = (ArrayList<Report>) getActivity().getIntent().getSerializableExtra("reports");

        testUser = (User) getActivity().getIntent().getSerializableExtra("user");
        welcome = (TextView) root.findViewById(R.id.welcome);

        welcome.setText("Welcome, "+ testUser.getEmail()+"!");
        mRecyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ReportsAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        ((ReportsAdapter) mAdapter).setOnItemClickListener(new ReportsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });

        plusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        return root;
    }

    public void selectImage(){
        Intent intent = new Intent (this.getContext(), ImageChooser.class);
        intent.putExtra("user",testUser);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ReportsAdapter) mAdapter).setOnItemClickListener(new ReportsAdapter
                .MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
            }
        });
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        list.clear();
        list.addAll(list);
        mAdapter.notifyDataSetChanged();
    }





}