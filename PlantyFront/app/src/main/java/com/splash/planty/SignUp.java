package com.splash.planty;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUp extends AppCompatActivity {


    Button sign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sign_up = (Button) findViewById(R.id.signing_up);


    }

    public void onRegister (View v){


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8000/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
       ApiService api = retrofit.create(ApiService.class);

        User u= new User();
        EditText email   = (EditText)findViewById(R.id.editText4);
        u.setEmail(email.getText().toString());
        EditText password   = (EditText)findViewById(R.id.editText5);
        u.setPassword(password.getText().toString());
        u.setLat(0.0);
        u.setLongitude(0.0);




        Call<UserResponse> call = api.registerUser(u);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

             if(response.body().getState().equalsIgnoreCase("ok")){
              //show message that registration was successful and go to login page

                 Toast.makeText(getApplicationContext(), "User registered sucessfully!", Toast.LENGTH_LONG).show();

                 openLoginActivity();


            } else {

                 //show message response.body().getState()

                 Toast.makeText(getApplicationContext(),response.body().getState() , Toast.LENGTH_LONG).show();;
             }

            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable throwable) {
                //show connection error message

                Toast.makeText(getApplicationContext(),throwable.getMessage(), Toast.LENGTH_LONG).show();;
            }
       });

    }

    private void openLoginActivity() {
        Intent intent = new Intent (this, MainActivity.class);
        startActivity(intent);
    }
}
