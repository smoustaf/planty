package com.splash.planty;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiService {

    @POST("users/")
    Call<UserResponse> registerUser(@Body User user);

    @POST("sessions/")
    Call<UserResponse> loginUser(@Body User user);


    @GET("users/{userid}/plants")
    Call<List<Report>> getReports(@Path("userid") int userid);
    @Multipart

    @POST("users/{userid}/plants/")
    Call<ResponseBody> addReport(@Path("userid") int userid, @Part("title") RequestBody title, @Part MultipartBody.Part image);
}
