package com.splash.planty;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageChooser extends AppCompatActivity {

    private int PICK_IMAGE_REQUEST = 3;
    ImageView choosen_picture;
    public String image_path="";
    Bitmap bitmap;

    private static int RESULT_LOAD_IMAGE = 1;
    static final Integer READ_EXST = 0x4;

    private final String[] REQUIRED_PERMISSIONS = new String[]{"Manifest.permission.READ_EXTERNAL_STORAGE","Manifest.permission.WRITE_EXTERNAL_STORAGE"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_chooser);
        choosen_picture  = (ImageView) findViewById(R.id.choosen_picture);
        choosen_picture.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.upload));

        choosen_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isStoragePermissionGranted()){
                    openGallery();
                }
                else{
                    ActivityCompat.requestPermissions(getParent(), REQUIRED_PERMISSIONS, READ_EXST);
                }
            }
        });


    }

    public void openGallery (){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();


            Log.i("Radwan",image_path);
            try {

                 bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);


//Convert bitmap to byte array

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                File f = new File(this.getCacheDir(), "temp.png");
                f.createNewFile();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(f);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                image_path= f.getAbsolutePath();
                choosen_picture.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permissions is granted",  Toast.LENGTH_LONG).show();
                return true;
            } else {

                Toast.makeText(this, "Permissions is revoked",  Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Toast.makeText(this, "Permissions is granted",  Toast.LENGTH_LONG).show();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this,  "Permission: "+permissions[0]+ "was "+grantResults[0],  Toast.LENGTH_LONG).show();
        }
    }



    public void scanImage(View view){

         User u = (User) this.getIntent().getSerializableExtra("user");

                    Log.i("RADWAN",image_path);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:8000/")
                        .addConverterFactory(GsonConverterFactory.create())
                       .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                       .build();
                ApiService api = retrofit.create(ApiService.class);
                File file_to_upload = new File(image_path);
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file_to_upload);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("image", file_to_upload.getName(), requestFile);

                //title not necessary now
                RequestBody title =
                        RequestBody.create(MediaType.parse("multipart/form-data"), "optional title");
                // VERY IMPORTANT REPLACE 1 with the id of the current user which you should pass all the way from the login page
               int userid=u.getId();
                Call<ResponseBody> call = api.addReport(userid,title,body);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                           String diagnosis = response.body().string();
                            Report report = new Report( Calendar.getInstance().getTime().toString(), image_path,diagnosis.replace("_"," "));
;                        Intent intent = new Intent (ImageChooser.this, Reports.class);
                            intent.putExtra("report", report);
                            intent.putExtra("image",bitmap);
                            startActivity(intent);
                            //diagnosis is the result of the classification
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                        Log.i("RADWAN", throwable.getMessage());
                        // connection error
                    }
                });
            }

//            @Override
//            public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
//                String msg = "Pic capture failed : " + message;
//                Toast.makeText(getBaseContext(), msg,Toast.LENGTH_LONG).show();
//                if(cause != null){
//                    cause.printStackTrace();
//                }
//            }
//        });

    }







