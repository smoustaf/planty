package com.splash.planty;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    RelativeLayout rellay1, rellay2;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            rellay1.setVisibility(View.VISIBLE);
            rellay2.setVisibility(View.VISIBLE);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rellay1 = (RelativeLayout) findViewById(R.id.rellay1);
        rellay2 = (RelativeLayout) findViewById(R.id.rellay2);

        handler.postDelayed(runnable, 2000);
    }

    public void goSignUp (View view){
        Intent intent = new Intent (this, SignUp.class);
        startActivity(intent);
    }

    public void goHome (View view){
        Retrofit retrofit = new Retrofit.Builder()
              .baseUrl("http://10.0.2.2:8000/")
                .addConverterFactory(GsonConverterFactory.create())
               .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        final ApiService api = retrofit.create(ApiService.class);

        User u= new User();
        EditText email   = (EditText)findViewById(R.id.LoginUsername);
        u.setEmail(email.getText().toString());
        EditText password   = (EditText)findViewById(R.id.LoginPassword);
        u.setPassword(password.getText().toString());

        Call<UserResponse> call = api.loginUser(u);
        call.enqueue(new Callback<UserResponse>() {
           @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                if(response.body().getState().equalsIgnoreCase("ok")){
                    //login is done
                    final int id = response.body().getId();
                    Log.i("login","Login success id is "+ id);
                    Toast.makeText(getApplicationContext(), "Login success", Toast.LENGTH_SHORT).show();

                    Call<List<Report>> call2 = api.getReports(id);
                    call2.enqueue(new Callback<List<Report>>() {
                        @Override
                        public void onResponse(Call<List<Report>> call, Response<List<Report>> response) {
                            User u1= new User();
                            EditText email   = (EditText)findViewById(R.id.LoginUsername);
                            u1.setEmail(email.getText().toString());
                            EditText password   = (EditText)findViewById(R.id.LoginPassword);
                            u1.setPassword(password.getText().toString());
                            u1.setId(id);
                            Intent intent = new Intent (MainActivity.this, Home.class);
                            intent.putExtra("user", u1);
                            intent.putExtra("reports", (ArrayList) response.body());

                            startActivity(intent);
                           //response.body gives back a list of reports for the user that just logged in
                           // here launch the ui that is supposed to show them

                        }
                        @Override
                       public void onFailure(Call<List<Report>> call, Throwable throwable) {
                           //connection error
                            Toast.makeText(getApplicationContext(), "failed to get reports", Toast.LENGTH_SHORT).show();
                       }
                    });

                }
               else{
                   Log.i("login",response.body().getState());
                    //login failed
                    Toast.makeText(getApplicationContext(), "failed to login", Toast.LENGTH_SHORT).show();
                }
            }
           @Override
            public void onFailure(Call<UserResponse> call, Throwable throwable) {
                // connection failure
                Log.i("login",throwable.getMessage());
               Toast.makeText(getApplicationContext(), "failed to login", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
