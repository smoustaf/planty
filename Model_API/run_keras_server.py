# USAGE
# Start the server:
# 	python run_keras_server.py
# Submit a request via cURL:
# 	curl -X POST -F image=@dog.jpg 'http://localhost:5000/predict'
# Submita a request via Python:
#	python simple_request.py

# import the necessary packages

import numpy as np
import pickle
import cv2
import keras
import tensorflow as tf
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from PIL import Image
import numpy as np
import flask
import io

app = flask.Flask(__name__)
model = None

def load_model():
	
	global model,label_transformer,graph

	graph = tf.get_default_graph()
	model = pickle.load(open( "cnn_model.pkl", "rb" ))
	label_transformer = pickle.load(open( "label_transform.pkl", "rb" ))


    
def prepare_image(image, target):
	# resize the input image and preprocess it
	if image.mode != "RGB":
		image = image.convert("RGB")

	image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
	image = cv2.resize(np.float32(image), target)
	image = img_to_array(image)


	return image

@app.route("/predict", methods=["POST"])
def predict():

	data = {"success": False}
	if flask.request.method == "POST":
		if flask.request.files.get("image"):
			image = flask.request.files["image"].read()
			image = Image.open(io.BytesIO(image))

			images = [prepare_image(image, target=tuple((256, 256)))]
			np_image_list = np.array(images, dtype=np.float16)/225.0
			with graph.as_default():
				result = model.predict(np_image_list)
			data["predictions"] = label_transformer.inverse_transform( result)[0]


			data["success"] = True

	return flask.jsonify(data)


if __name__ == "__main__":
	print(("* Loading Keras model and Flask starting server..."
		"please wait until server has fully started"))
	load_model()
	app.run()