import requests
import json

#CREATING TEST USERS
# requests.put('http://127.0.0.1:8000/users/', data=json.dumps({'email':'test@gmail.com', 'password':'abc', 'first_name':"the", 'last_name':'test', 'long':2.0, 'lat':5.0}))
# requests.put('http://127.0.0.1:8000/users/', data=json.dumps({'email':'test2@gmail.com', 'password':'abc', 'first_name':"test", 'last_name':'two', 'long':2.0, 'lat':5.0}))

#LOGIN A USER
s = requests.Session()
# s.post('http://127.0.0.1:8000/sessions/', data=json.dumps({'username':'test@gmail.com', 'password':'abc'}))
s.post('http://127.0.0.1:8000/sessions/', data=json.dumps({'email':'test2@gmail.com', 'password':'abc'}))

#LOGOUT A USER
# requests.delete('http://127.0.0.1:8000/sessions/', data=json.dumps({'username':'test@gmail.com', 'password':'abc'}))


#CHECK USER DETAILS
# requests.get('http://127.0.0.1:8000/users/2/')

#LIST ALL USERS
# requests.get('http://127.0.0.1:8000/users/')

#CREATE A PLANT UNDER A USER
# s.put('http://127.0.0.1:8000/users/2/plants/', data=json.dumps({'title':'tomato', 'harvested_date':'', 'quality':"1", 'active':True}))
# s.put('http://127.0.0.1:8000/users/2/plants/', data=json.dumps({'title':'apple', 'harvested_date':'', 'quality':"1", 'active':True}))
s.put('http://127.0.0.1:8000/users/3/plants/', data=json.dumps({'title':'chickpeas', 'harvested_date':'', 'quality':"1", 'active':True, 'diagnosis': "TEST11"}))

#GET A PLANT UNDER A SPECIFIC USER
#THIS SHOULD BE UNAUTHORIZED
# s.get('http://127.0.0.1:8000/users/1/plants/2/')
# #THIS SHOULD BE AUTHORIZED
# s.get('http://127.0.0.1:8000/users/2/plants/2/')