from django.conf.urls import url
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
	url(r'^sessions/$', views.session, name='session'),
    url(r'^users/$', views.appUser, name='appUser'),
    url(r'^users/(?P<id>\w+)/$', views.userDetails, name='userDetails'),
    url(r'^users/(?P<id>\w+)/plants/$', views.appPlant, name='appPlant'),
    url(r'^users/(?P<id>\w+)/plants/(?P<plantid>\w+)/$', views.plantDetails, name='plantDetails'),
    
]
urlpatterns +=  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# url(r'^users/(?P<id>\w+)/plants/active=<int:active>/$', views.plantActivity, name='plantActivity')