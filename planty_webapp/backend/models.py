from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class PlantyUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #REMOVED, Exist in the builtin model.
    #####################################
    # email = models.EmailField(max_length=254)
    # password = models.TextField()
    # created_date = models.DateTimeField(default=timezone.now)
    ##CHANGED. DecimalField supports fast correctly-rounded decimal floating point arithmetic
    ######################################################################
    # lat = models.FloatField(blank=True, null=True)
    # long = models.FloatField(blank=True, null=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    long = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    
    # def __str__(self):
    #     return self.email




class Report(models.Model):
     user= models.ForeignKey(PlantyUser, on_delete=models.CASCADE)
     created_date = models.DateTimeField(auto_now_add=True)
     image = models.ImageField(upload_to="")
     diagnosis= models.TextField(max_length=500)
     
     
     

        
