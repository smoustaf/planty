from django.shortcuts import render
import json
from django.conf import settings
from django.core import serializers
from django.contrib.auth import authenticate, login, logout, get_user
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import (PlantyUser, Report)
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, FileResponse
import requests

# Create your views here.
@csrf_exempt 
def session(req):
   
    print(req.body)
    #TEST GET
    if req.method == 'GET':
        return HttpResponse('ok', status=200)
    if req.method == 'POST':
        text = json.loads(req.body.decode())
        print('__________LOGIN __________')
        username = text['email']
        password = text['password']
        user = authenticate(username=username, password=password)
        print(req.user.username)
        print(user.is_authenticated)
        if user is not None:
            login(req, user)
            return HttpResponse(json.dumps({"state":"ok","id":user.pk}), status=200)
        else:
            return HttpResponse({"state":"login failed"}, status=401)
    if req.method == 'DELETE':
        logout(req)
        return HttpResponse('Successful logout', status=200)

@csrf_exempt 
def appUser(req):
    print(req.body)
    #return a json of all Users
    ##PlantyUsers only passed as result for efficiency purposes for now.
    if req.method == 'GET':
        users = serializers.serialize('json', PlantyUser.objects.all())
        return HttpResponse(json.dumps(users), content_type="application/json")
        # return HttpResponse('GET TEST', status=200)
    if req.method == 'POST':
        #get request attributes
        put_data = json.loads(req.body.decode())
        userPass = put_data['password']
        email = put_data['email']
        lat = put_data['lat']
        long = put_data['long']
        username = email

        #check if user email already exists
        if User.objects.filter(email=email).exists():
            result = {'state': 'Email already exists', "id":None}
            return HttpResponse(
                json.dumps(result), content_type="application/json")
        
        #create builtin user and plantyUser
        DjangoUser = User.objects.create_user(username, email, userPass)
        DjangoUser.save()

        plantyUser = PlantyUser(user= DjangoUser, lat = 1.11, long=2.22)
        plantyUser.save()
        
        return HttpResponse(json.dumps({"id":DjangoUser.pk ,'state': "ok"}), content_type="application/json")  

#IS login actually required?        
# @login_required
def userDetails(req, id):
    #Result is passed as 2 jsons, one for user and plantyUser
    userID = int(id)
    if req.method == 'GET':
        user1 = PlantyUser.objects.get(pk=userID)
        user2 = User.objects.get(pk=userID)
        users = serializers.serialize('json', [user1,user2])
        return HttpResponse(json.dumps(users), content_type="application/json")
    if req.method == 'PATCH':
        patch_data = json.loads(req.body.decode())
        user1 = PlantyUser.objects.get(pk=userID)
        user2 = User.objects.get(pk=userID)

        user1.lat = patch_data['lat']
        user1.long = patch_data['long']
        user2.set_password(patch_data['password'])
        user1.save()
        user2.save()
        return HttpResponse('User Edit was successful!', status=200)

    return HttpResponse('GET TEST', status=200)

# @login_required
@csrf_exempt 
def appPlant(req, id):
    user = PlantyUser.objects.get(pk=id)
    # DO we Allow repeated plant names???
    if req.method == 'POST':
        
        #get request attributes
        put_data =req.POST
        
        #TODO: Make sure date fields was created correctly!
        
        KERAS_REST_API_URL = "http://localhost:5000/predict"
        report = Report(user=user, image = req.FILES['image'], diagnosis = "pending")
        report.save()
        IMAGE_PATH = report.image.path
        image = open(IMAGE_PATH, "rb").read()
        payload = {"image": image}
        r = requests.post(KERAS_REST_API_URL, files=payload).json()
        if r["success"]:
            report.diagnosis=r["predictions"]
            report.save()
            return HttpResponse(report.diagnosis, status=200)
        # print("IT SHOULD HAVE SAVED NOW!")
        return HttpResponse('Success', status=200)
    if req.method == 'GET':
        #if authenticated user has the same ID as "user"
        #NEW
        reports = Report.objects.filter(user = user)
        sentJson = []
        for report in reports:
           
            print (report)
            sentJson.append({"image":settings.MEDIA_ROOT+report.image.name,"created_date":str( report.created_date),"diagnosis":report.diagnosis})
        print(sentJson)
       
        return HttpResponse(json.dumps(sentJson), content_type="application/json")

@login_required
def plantDetails(req,id,plantid):
    if req.method == 'GET':
        current_user = req.user.id
        plant = Plant.objects.get(pk = plantid)
        #NEW
        reports = Report.object.filter(plant = plant)
        #check if the logged in user is the plant creator
        # print(plant.user.id)
        print(req.user)
        
        if plant.user.id == id:
            print(plant.user.id)
            print(current_user)
            #sending the plant along with a list of its reports
            p = serializers.serialize('json', [plant, reports])
            return HttpResponse(json.dumps(p), content_type="application/json")
        else:
            return HttpResponse('Cannot find a plant with this ID under current user', status=401)

# @login_required
# def plantActivity(req, id):
#     if req.method == 'GET':
#         current_user = req.user.id
#         user = PlantyUser.objects.get(pk=id)
#         plants = Plant.objects.filter(user = user)
#         #get a set of active and inactive plants
#         active = []
#         inactive = []
#         for plant in plants:
#             print(plant)
#             if plant.active:
#                 active.append(plant)
#             else:
#                 inactive.append(plant)
